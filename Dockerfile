FROM ubuntu:rolling

LABEL maintainer="carsakiller <carsakiller@gmail.com>"
LABEL description="Docker image for testing Lua using Busted and LuaCov"

ARG LUA_VERSION=
ARG LUAROCKS_VERSION=

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libreadline-dev \
    curl \
    git \
    unzip \
    zip \
    && rm -rf /var/lib/apt/lists/*

# Install Lua or LuaJIT based on the specified version
RUN <<EOF
if [ "${LUA_VERSION}" = "jit" ]; then
    # Install LuaJIT
    git clone https://luajit.org/git/luajit.git
    cd luajit
    make
    make install
    cd ..
    rm -rf luajit;
else
    # Install Lua
    curl -R -O https://www.lua.org/ftp/lua-${LUA_VERSION}.tar.gz
    tar -zxf lua-${LUA_VERSION}.tar.gz
    cd lua-${LUA_VERSION}
    make linux test
    make install
    cd ..
    rm -rf lua-${LUA_VERSION}*;
fi
EOF

# Install LuaRocks
RUN <<EOF
curl -R -O https://luarocks.github.io/luarocks/releases/luarocks-${LUAROCKS_VERSION}.tar.gz
tar -zxf luarocks-${LUAROCKS_VERSION}.tar.gz
cd luarocks-${LUAROCKS_VERSION}
if [ "${LUA_VERSION}" = "jit" ]; then
    ./configure --with-lua-interpreter=luajit;
else
    ./configure --with-lua-include=/usr/local/include;
fi
make
make install
rm -rf luarocks-${LUAROCKS_VERSION}*
EOF

# Install LuaCov from source
RUN git clone https://github.com/lunarmodules/luacov.git \
    && cd luacov \
    && luarocks make \
    && cd .. \
    && rm -rf luacov

# Install rocks
RUN luarocks install busted
RUN luarocks install luacov-console

WORKDIR /app

COPY test.sh /test.sh
RUN chmod +x /test.sh

CMD [ "/test.sh" ]

