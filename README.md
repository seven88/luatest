# LuaTest Docker Image

A simple Docker image intended for testing Lua code using [Busted][Busted] and [LuaCov][LuaCov] - also contains [LuaRocks][LuaRocks]. It can be used for installing and publishing rocks to LuaRocks as well.

## Tags

The tags are kept semi-manually up to date, with one tag per minor Lua version. While the `luaJIT` tag is always built from the latest source, the others are the latest patch version (e.g. `5.3.6`).

- `lua5.4`
- `lua5.3`
- `lua5.1`
- `luaJIT`

## Usage

While testing Lua via [Busted][Busted] is the intended use, it can be used as a general Lua/LuaRocks container.

### Testing with Busted

The container will automatically run [test.sh](./test.sh). The below ENV variables can be used to configure the script.

|      ENV VAR       |   Default   |                                                                                                                       |
| :----------------: | :---------: | --------------------------------------------------------------------------------------------------------------------- |
|      SRC_DIR       |     ./      | Directory where your source code can be found                                                                         |
|      TEST_DIR      |    spec     | Directory where your test files can be found                                                                          |
|   BUSTED_PATTERN   |   \_spec    | [Pattern](https://lunarmodules.github.io/busted/#:~:text=%2Dp%2C%20%2D%2Dpattern%3DPATTERN) of test files             |
|   BUSTED_OUTPUT    | utfTerminal | Busted [output handler](https://lunarmodules.github.io/busted/#output-handlers) to use                                |
|  LUACOV_REPORTER   |    none     | Reporter(s) to use for LuaCov. Can be `"none"`, `"html"`, `"console"`, or `"html+console"`                            |
| LUACOV_CONFIG_PATH |             | Where your [LuaCov config file](https://github.com/lunarmodules/luacov?tab=readme-ov-file#configuration) can be found |

#### Docker Run

Here are a few examples of using LuaTest with Docker run to quickly test the code in your current directory.

<details open>

<summary>Simple Busted test</summary>

```sh
docker run --rm -t -v ./:/app carsakiller/luatest:lua5.4
```

> Note: `-t` provides a pseudo-TTY so the colors of the `utfTerminal` output show up correctly.

</details>

<details>

<summary>Busted test on <code>tests/</code></summary>

```sh
docker run --rm -t -v ./:/app \
  -e SRC_DIR="src" \
  -e TEST_DIR="tests" \
  -e BUSTED_PATTERN="_test" \
  carsakiller/luatest:lua5.4
```

</details>

<details>

<summary>Busted test with coverage</summary>

```sh
docker run --rm -t -v ./:/app \
  -e LUACOV_REPORTER="html+console" \
  -e LUACOV_CONFIG_PATH=".luacov.lua" \
  carsakiller/luatest:lua5.4
```

</details>

#### GitLab CI

Below are some examples for using LuaTest in GitLab CI pipelines 🚀.

<details open>

<summary>Simple test pipeline</summary>

```yaml
stages:
  - test

test:
  stage: test
  image: carsakiller/luatest:lua5.4
  variables:
    BUSTED_OUTPUT: TAP
  script:
    - /test.sh
```

> Note: a `script` command is required by GitLab

</details>

<details>

<summary>Test and coverage pipeline</summary>

```yaml
stages:
  - test

test:
  stage: test
  image: carsakiller/luatest:lua5.4
  variables:
    BUSTED_OUTPUT: TAP
    LUACOV_REPORTER: "html+console"
    LUACOV_CONFIG_PATH: ".luacov.ci.lua" # You may want a luacov config for CI specifically
  script:
    - /test.sh
  coverage: '/([\d\.]+)% Coverage/' # Capture coverage percentage with regexp for GitLab
  artifacts:
    when: always
    untracked: true
```

</details>

<details>

<summary>Test and cover all Lua versions</summary>

```yaml
stages:
  - test

test:
  stage: test
  parallel:
    matrix:
      - LUA_VERSION: lua5.1
      - LUA_VERSION: lua5.3
      - LUA_VERSION: lua5.4
      - LUA_VERSION: luaJIT
  image: carsakiller/luatest:${LUA_VERSION}
  variables:
    BUSTED_OUTPUT: TAP
    LUACOV_REPORTER: "html+console"
    LUACOV_CONFIG_PATH: ".luacov.ci.lua"
  script:
    - /test.sh
  coverage: '/([\d\.]+)% Coverage/'
  artifacts:
    when: always
    untracked: true
```

</details>

### Publishing Lua Rock

For publishing a Lua rock, you will need an [API key from LuaRocks](https://github.com/luarocks/luarocks/wiki/upload).

```sh
docker run --rm -v ./:/app \
  --entrypoint /bin/bash \
  carsakiller/luatest:lua5.4 \
  -c luarocks upload $(find . -name "*.rockspec") --api-key=$LUAROCKS_API_KEY
```

#### GitLab CI

Here is a full bells and whistles example of testing, packing, and uploading a rock to GitLab packages and LuaRocks, and then creating a GitLab release. Your LuaRocks API key can be provided to the job as a [CI variable](https://docs.gitlab.com/ee/ci/variables/). You will probably want to protect, mask, and hide the variable to prevent it leaking. Be careful that your CI job is not modified to do anything nefarious with the key.

```yaml
# We only want to release if the tag is a version like 0.0.0
# You may want to modify the below regex to match your tag versioning style
.if_release_tag: &if_release_tag
  if: $CI_COMMIT_TAG =~ /^\d.\d.\d$/

stages:
  - test
  - build
  - release

test:
  stage: test
  parallel:
    matrix:
      - LUA_VERSION: lua5.1
      - LUA_VERSION: lua5.3
      - LUA_VERSION: lua5.4
      - LUA_VERSION: luaJIT
  image: carsakiller/luatest:${LUA_VERSION}
  variables:
    BUSTED_OUTPUT: TAP
    LUACOV_REPORTER: "html+console"
    LUACOV_CONFIG_PATH: ".luacov.ci.lua"
  script:
    - /test.sh
  coverage: '/([\d\.]+)% Coverage/'
  artifacts:
    when: always
    untracked: true

pack:
  stage: build
  image: carsakiller/luatest:5.4
  rules:
    - *if_release_tag
  needs:
    - job: test
      artifacts: false
  script:
    - echo "ROCKSPEC_FILE=$(find . -name\"*.rockspec\")" >> variables.env # We will need some vars in the release step
    - luarocks pack $(find . -name "*.rockspec")
    - echo "SRCROCK_FILE=$(find . -name\"*.src.rock\")" >> variables.env
  artifacts:
    reports:
      dotenv: variables.env
    paths:
      - "*.src.rock" # Save src rock as artifact for other jobs
    when: on_success
    access: all
    expire_in: "7 days"

upload-rock:
  stage: release
  image: carsakiller/luatest:5.4
  rules:
    - *if_release_tag
  dependencies:
    - pack
  script:
    # Upload to GitLab packages repository for this project
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $SRCROCK_FILE "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${CI_COMMIT_TAG}/${SRCROCK_FILE}"'
    - luarocks upload ${ROCKSPEC_FILE} --api-key=$LUAROCKS_API_KEY

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - *if_release_tag
  needs:
    - job: upload-rock
      artifacts: false
    - job: pack
      artifacts: true
  script:
    - echo "Creating release..."
  release:
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG_MESSAGE"
    assets:
      links:
        - name: "${SRCROCK_FILE}"
          url: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${CI_COMMIT_TAG}/${SRCROCK_FILE}
          link_type: package
```

## Build

### Arguments

#### `LUA_VERSION`

The version of Lua to install. e.g. `5.4.7`, `jit`

#### `LUAROCKS_VERSION`

The version of LuaRocks to install. e.g. `3.11.1`

### Example

```sh
docker build -t docker.io/carsakiller/luatest:lua5.4 --build-arg LUA_VERSION=5.4.7 --build-arg LUAROCKS_VERSION=3.11.1 .
docker build -t docker.io/carsakiller/luatest:luaJIT --build-arg LUA_VERSION=jit --build-arg LUAROCKS_VERSION=3.11.1 .
```

[Busted]: https://lunarmodules.github.io/busted
[LuaCov]: https://lunarmodules.github.io/luacov/
[LuaRocks]: https://luarocks.org/
