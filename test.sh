#!/bin/bash

set -e

# Busted config
SRC_DIR=${SRC_DIR:-"./"}
TEST_DIR=${TEST_DIR:-spec}
BUSTED_PATTERN=${BUSTED_PATTERN:-"_spec"}
BUSTED_OUTPUT=${BUSTED_OUTPUT:-"utfTerminal"}

# LuaCov config
LUACOV_REPORTER=${LUACOV_REPORTER:-"none"}
LUACOV_CONFIG_PATH=${LUACOV_CONFIG_PATH}

# Add --coverage flag if a coverage reporter is provided
COVERAGE_FLAG=""
if [ "$LUACOV_REPORTER" != "none" ]; then
    COVERAGE_FLAG="--coverage"
fi

busted --pattern="$BUSTED_PATTERN" --output="$BUSTED_OUTPUT" $COVERAGE_FLAG "$TEST_DIR" $@

LUACOV_CONFIG_FLAG=""
if [ "$LUACOV_CONFIG_PATH" ]; then
    LUACOV_CONFIG_FLAG="-c=$LUACOV_CONFIG_PATH"
fi

if [[ "$LUACOV_REPORTER" == "html" || "$LUACOV_REPORTER" == *"html"* ]]; then
    HTML_REPORT_PATH="luacov.report.html"

    echo -e "\nGenerating HTML coverage report..."
    luacov -r="html" $SRC_DIR $LUACOV_CONFIG_FLAG
    mv luacov.report.out $HTML_REPORT_PATH
    echo "HTML report exported to $HTML_REPORT_PATH"

    # Only log percentage if console reporter is not also selected. It will log coverage %
    if [ !"$LUACOV_REPORTER" == *"console"* ]; then
        awk -F'[<>]' '/<span><strong>/ {print "\n" $5 " Coverage"; exit}' $HTML_REPORT_PATH
    fi
fi

if [[ "$LUACOV_REPORTER" == "console" || "$LUACOV_REPORTER" == *"console"* ]]; then
    echo -e "\nGenerating console report...\n"
    luacov-console $SRC_DIR

    # Print summary then strip ANSI and print total coverage % for CI
    luacov-console -s | tee >(tail -n 1 | awk '{print "\n" $NF" Coverage"}' | sed 's/\x1B\[[0-9;]*[mK]//g')
fi